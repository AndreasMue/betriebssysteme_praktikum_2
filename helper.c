/* Helper-Quelldatei
*  beinhaltet die kritischen Abschnitte
*/

#include <semaphore.h>
#include "header.h"




#define WORKOUT_LOOP 500000000
#define REST_LOOP 1000000000
#define STRLEN_AUSGABE 124


// Prototypen
void get_weights (struct philosoph *data);
void workout (struct philosoph *data);
void put_weights (struct philosoph *data);
void rest (struct philosoph *data);
void display_status ();


/*
 * Gewichte holen
 * 
 * !!!!! KRITISCHER ABSCHNITT !!!!!
 * 
 * -> Prüfen ob Gewichtkombination verfügbar:
 *   ja   -> Gewicht "nehmen"
 *   nein -> Blockieren (condition variable)
 */
void get_weights (struct philosoph *data) 
{ 
    // Auf Mutex blockieren
    pthread_mutex_lock(data->mutex);
    // Zustand = Get Weights
    data->zustand = 'G';
    // --Display Status
    display_status();
    // Verfügbarkeit der Gewichte prüfen
    int i;
    for (i=0; i<KOMBO_COUNT; i++) 
    {
        if (gewicht_kombo[i].kombo_gesamtgewicht == data->soll_gewicht)
        {
            if (ist_gewichte.count_2kg >= gewicht_kombo[i].kombo.count_2kg &&
                ist_gewichte.count_3kg >= gewicht_kombo[i].kombo.count_3kg &&
                ist_gewichte.count_5kg >= gewicht_kombo[i].kombo.count_5kg)
            {
                // Ist Gewichte anpassen
                ist_gewichte.count_2kg -= gewicht_kombo[i].kombo.count_2kg;
                ist_gewichte.count_3kg -= gewicht_kombo[i].kombo.count_3kg;
                ist_gewichte.count_5kg -= gewicht_kombo[i].kombo.count_5kg;
                
                // Vom Philsophen Gewichte anpassen
                data->genutzt.count_2kg = gewicht_kombo[i].kombo.count_2kg;
                data->genutzt.count_3kg = gewicht_kombo[i].kombo.count_3kg;
                data->genutzt.count_5kg = gewicht_kombo[i].kombo.count_5kg;

                //           Mutex freigeben
                pthread_mutex_unlock(data->mutex);

                i = KOMBO_COUNT;                 	
                //           --Workout
            }  
            else 
            {
                if(i == (KOMBO_COUNT-1) )
                {
                    // Keine Kombination verfügbar, blockieren!
                    data->befehls_status = 'b';
                    pthread_cond_wait(&cond_var, data->mutex);
                    data->befehls_status = 'n';
                    // Neuer Versuch, Gewichte zu holen
                    i = 0;
                }
            }                   
        }
        else 
        {
            if(i == (KOMBO_COUNT-1) )
            {
                // Keine Kombination verfügbar, blockieren!
                data->befehls_status = 'b';
                pthread_cond_wait(&cond_var, data->mutex);
                data->befehls_status = 'n';
                // Neuer Versuch, Gewichte zu holen
                i = 0;
            }
        } 
           
    }
}



void workout (struct philosoph *data)
{
    pthread_mutex_lock(data->mutex);
    // Zustand = Workout
    data->zustand = 'W';
    // --Display Status   
    display_status();
    pthread_mutex_unlock(data->mutex);
  
    int i;
    
    for (i=0; i<WORKOUT_LOOP; i++)
    {
        // Steueranweisungen ausführen
        if (steuerarray[ data->id ] != '0')
        {
            char befehl = steuerarray[ data->id ];
            int returnvalue;
            switch (befehl)
            {
                case 'b': data->befehls_status = 'b';
                          sem_wait(&semaphoren[ data->id ]);                     
                          if (steuerarray[ data->id ] == 'q')
                          {
                              pthread_exit(&returnvalue);
                          }
                          data->befehls_status = 'n';
                          break;
                case 'p': i = WORKOUT_LOOP;
                          break;
                case 'q': pthread_exit(&returnvalue);
                          break;
            }
            
            steuerarray[ data->id ] = '0';
        }
    }
    
    // --Put Weights
}



/*
 * Gewichte zurückgeben
 * 
 * !!!!! KRITISCHER ABSCHNITT !!!!!
 * 
 * -> Gewichte "zurücklegen"
 */
void put_weights (struct philosoph *data)
{
    // Auf Mutex blockieren
    pthread_mutex_lock(data->mutex);
    // Zustand = Put Weights
    data->zustand = 'P';
    
    //           --Display Status
    display_status();
    // Gewichte zurücklegen
    ist_gewichte.count_2kg += data->genutzt.count_2kg;
    ist_gewichte.count_3kg += data->genutzt.count_3kg;
    ist_gewichte.count_5kg += data->genutzt.count_5kg;
    
    data->genutzt.count_2kg = 0;
    data->genutzt.count_3kg = 0;
    data->genutzt.count_5kg = 0;
    // Threads wecken (Signal Condition)
    pthread_cond_broadcast(&cond_var);
    // Mutex freigeben
    pthread_mutex_unlock(data->mutex);
    // --Rest
}



void rest (struct philosoph *data)
{
    pthread_mutex_lock(data->mutex);
    // Zustand = Rest
    data->zustand = 'R';
    // --Display Status
    
    display_status();
    pthread_mutex_unlock(data->mutex);
    
    int i;
    
    for (i=0; i<REST_LOOP; i++)
    {
        // Steueranweisungen ausführen
        if (steuerarray[ data->id ] != '0')
        {
            char befehl = steuerarray[ data->id ];
            int returnvalue;
            switch (befehl)
            {
                case 'b': data->befehls_status = 'b';
                          sem_wait(&semaphoren[ data->id ]);
                          if (steuerarray[ data->id ] == 'q')
                          {
                              pthread_exit(&returnvalue);
                          }
                          data->befehls_status = 'n';
                          break;
                case 'p': i = REST_LOOP;
                          break;
                case 'q': pthread_exit(&returnvalue);
                          break;
            }
            
            steuerarray[ data->id ] = '0';
        }
    }
    
    // --Get Weights
}



void display_status ()
{
    // Kontrolle, ob Gewicht-Gesamtmenge noch stimmt
    //   false-> Ausgabe eines Alarmes!
    
    struct gewichte current_gewichte;
    current_gewichte.count_2kg = 0;
    current_gewichte.count_3kg = 0;
    current_gewichte.count_5kg = 0;
    
    
    // Ausgaben des derzeitigen Status aller Threads 
    int i;
    for (i=0; i<NUM_THREADS; i++)
    {
        // Philodaten
        int id = philosophen[i].id;
        unsigned int soll_g = philosophen[i].soll_gewicht;
        char bef_sta = philosophen[i].befehls_status;
        char zusta = philosophen[i].zustand;
        unsigned int g_2 = philosophen[i].genutzt.count_2kg;
        unsigned int g_3 = philosophen[i].genutzt.count_3kg;
        unsigned int g_5 = philosophen[i].genutzt.count_5kg;
        
        current_gewichte.count_2kg += g_2;
        current_gewichte.count_3kg += g_3;
        current_gewichte.count_5kg += g_5;
        
        
        printf("%d(%d)%c:%c:[ %d, %d, %d ] ", id, soll_g, bef_sta, zusta, g_2, g_3, g_5);
    }
    
    // Gesamtdaten
    unsigned int g_2 = ist_gewichte.count_2kg;
    unsigned int g_3 = ist_gewichte.count_3kg;
    unsigned int g_5 = ist_gewichte.count_5kg;
    
    printf("  Supply: [ %d, %d, %d ]\n", g_2, g_3, g_5);
    
    if (current_gewichte.count_2kg > max_gewichte.count_2kg ||
        current_gewichte.count_3kg > max_gewichte.count_3kg ||
        current_gewichte.count_5kg > max_gewichte.count_5kg)
    {
        perror ("!!!!!!!!!! ALARM: GESAMTGEWICHT ÜBERSTEIGT MAXIMALGEWICHT !!!!!!!!!!");
    }
    
}