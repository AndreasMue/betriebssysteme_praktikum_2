prog : main.o helper.o
	gcc -g -o prog main.o helper.o -pthread -lpthread

main.o : main.c header.h
	gcc -c -pthread -g -lpthread main.c

helper.o : helper.c header.h
	gcc -c -pthread -g -lpthread helper.c

clean :
	rm prog main.o helper.o
