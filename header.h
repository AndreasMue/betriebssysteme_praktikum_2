#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <semaphore.h>


#define KOMBO_COUNT 14
#define NUM_THREADS 5

#ifndef MAIN_FILE
#define EXTERN extern
#else
#define EXTERN
#endif
EXTERN struct gewichte
{
    unsigned int count_2kg;
    unsigned int count_3kg;
    unsigned int count_5kg;
};

EXTERN struct philosoph 
{
    unsigned int id;
    char befehls_status;
    char zustand;
    unsigned int  soll_gewicht;
    struct gewichte genutzt;
    pthread_mutex_t *mutex;
};

EXTERN char steuerarray[NUM_THREADS];

EXTERN pthread_mutex_t mutex;
EXTERN sem_t semaphoren [NUM_THREADS];

// Gewichte und Kombinationen
EXTERN struct gewichte ist_gewichte;
EXTERN struct gewicht_kombinationen
{
    struct gewichte kombo;
    unsigned int kombo_gesamtgewicht;
};
EXTERN struct gewicht_kombinationen gewicht_kombo [KOMBO_COUNT];

// Condition Variables
EXTERN pthread_cond_t cond_var;

EXTERN struct philosoph philosophen[NUM_THREADS];

EXTERN struct gewichte max_gewichte;

