#define MAIN_FILE
/* Main-Quelldatei */

#include <time.h>

#include "header.h"


// Externe Funktopnen deklarieren
extern void get_weights (struct philosoph *data);
extern void workout (struct philosoph *data);
extern void put_weights (struct philosoph *data);
extern void rest (struct philosoph *data);


// Threadmethode der Philosophen
void *philothread(void *arg);


// Threadobjekte
pthread_t philo_threads [NUM_THREADS];
pthread_barrier_t barriere;


/*
 * Main-Methode
 * 
 * Erstellen aller Synchronisationsobjekte
 * Erstellen aller Threads
 * Einlesen von Benutzerbefehlen 
 * 
 */
int main(void)
{ 
    // Kontrollvariable
    int result = 0;
    
    // Steuerarray
    steuerarray[0] = '0';
    steuerarray[1] = '0';
    steuerarray[2] = '0';
    steuerarray[3] = '0';
    steuerarray[4] = '0';

    // Gewichte erzeugen
    ist_gewichte.count_2kg = 4;
    ist_gewichte.count_3kg = 4;
    ist_gewichte.count_5kg = 5;
    
    max_gewichte.count_2kg = 4;
    max_gewichte.count_3kg = 4;
    max_gewichte.count_5kg = 5;
    
    gewicht_kombo[0].kombo.count_2kg = 0;
    gewicht_kombo[0].kombo.count_3kg =  2;
    gewicht_kombo[0].kombo.count_5kg =  0;
    gewicht_kombo[0].kombo_gesamtgewicht = 6;
    
    gewicht_kombo[1].kombo.count_2kg = 3;
    gewicht_kombo[1].kombo.count_3kg =  0;
    gewicht_kombo[1].kombo.count_5kg =  0;
    gewicht_kombo[1].kombo_gesamtgewicht = 6;
    
    gewicht_kombo[2].kombo.count_2kg = 0;
    gewicht_kombo[2].kombo.count_3kg =  1;
    gewicht_kombo[2].kombo.count_5kg =  1;
    gewicht_kombo[2].kombo_gesamtgewicht = 8;
    
    gewicht_kombo[3].kombo.count_2kg = 1;
    gewicht_kombo[3].kombo.count_3kg =  2;
    gewicht_kombo[3].kombo.count_5kg =  0;
    gewicht_kombo[3].kombo_gesamtgewicht = 8;
    
    gewicht_kombo[4].kombo.count_2kg = 4;
    gewicht_kombo[4].kombo.count_3kg =  0;
    gewicht_kombo[4].kombo.count_5kg =  0;
    gewicht_kombo[4].kombo_gesamtgewicht = 8;
    
    gewicht_kombo[5].kombo.count_2kg = 1;
    gewicht_kombo[5].kombo.count_3kg =  0;
    gewicht_kombo[5].kombo.count_5kg =  2;
    gewicht_kombo[5].kombo_gesamtgewicht = 12;
    
    gewicht_kombo[6].kombo.count_2kg = 2;
    gewicht_kombo[6].kombo.count_3kg =  1;
    gewicht_kombo[6].kombo.count_5kg =  1;
    gewicht_kombo[6].kombo_gesamtgewicht = 12;
    
    gewicht_kombo[7].kombo.count_2kg = 4;
    gewicht_kombo[7].kombo.count_3kg =  0;
    gewicht_kombo[7].kombo.count_5kg =  4;
    gewicht_kombo[7].kombo_gesamtgewicht = 12;
    
    gewicht_kombo[8].kombo.count_2kg = 3;
    gewicht_kombo[8].kombo.count_3kg =  2;
    gewicht_kombo[8].kombo.count_5kg =  0;
    gewicht_kombo[8].kombo_gesamtgewicht = 12;
    
    gewicht_kombo[9].kombo.count_2kg = 2;
    gewicht_kombo[9].kombo.count_3kg =  0;
    gewicht_kombo[9].kombo.count_5kg =  2;
    gewicht_kombo[9].kombo_gesamtgewicht = 14;
    
    gewicht_kombo[10].kombo.count_2kg = 0;
    gewicht_kombo[10].kombo.count_3kg =  3;
    gewicht_kombo[10].kombo.count_5kg =  1;
    gewicht_kombo[10].kombo_gesamtgewicht = 14;
    
    gewicht_kombo[11].kombo.count_2kg = 3;
    gewicht_kombo[11].kombo.count_3kg =  1;
    gewicht_kombo[11].kombo.count_5kg =  1;
    gewicht_kombo[11].kombo_gesamtgewicht = 14;
    
    gewicht_kombo[12].kombo.count_2kg = 1;
    gewicht_kombo[12].kombo.count_3kg =  4;
    gewicht_kombo[12].kombo.count_5kg =  0;
    gewicht_kombo[12].kombo_gesamtgewicht = 14;
    
    gewicht_kombo[13].kombo.count_2kg = 4;
    gewicht_kombo[13].kombo.count_3kg =  2;
    gewicht_kombo[13].kombo.count_5kg =  0;
    gewicht_kombo[13].kombo_gesamtgewicht = 14;
    
    result = pthread_mutex_init(&mutex, NULL);
    if(result != 0){
       perror("Mutex initialization failed");
       exit(EXIT_FAILURE);
    }
    
    // Semaphore Initialisierung   
    int k;
    for (k=0; k<NUM_THREADS; k++)
    {
        result = sem_init(&semaphoren[k], 0, 0);
        if(result != 0){
            perror("Semaphore initialization failed");
            exit(EXIT_FAILURE);
        }
    }
    
    // Strukturen der Philosophen erzeugen
    philosophen[0].id =  0;
    philosophen[0].befehls_status =  'n';
    philosophen[0].zustand = ' ';
    philosophen[0].soll_gewicht = 6;
    philosophen[0].genutzt.count_2kg = 0;
    philosophen[0].genutzt.count_3kg = 0;
    philosophen[0].genutzt.count_5kg = 0;
    philosophen[0].mutex = &mutex;
    
    philosophen[1].id =  1;
    philosophen[1].befehls_status =  'n';
    philosophen[1].zustand = ' ';
    philosophen[1].soll_gewicht = 8;
    philosophen[1].genutzt.count_2kg = 0;
    philosophen[1].genutzt.count_3kg = 0;
    philosophen[1].genutzt.count_5kg = 0;
    philosophen[1].mutex = &mutex;
    
    philosophen[2].id =  2;
    philosophen[2].befehls_status =  'n';
    philosophen[2].zustand = ' ';
    philosophen[2].soll_gewicht = 12;
    philosophen[2].genutzt.count_2kg = 0;
    philosophen[2].genutzt.count_3kg = 0;
    philosophen[2].genutzt.count_5kg = 0;
    philosophen[2].mutex = &mutex;
    
    philosophen[3].id =  3;
    philosophen[3].befehls_status =  'n';
    philosophen[3].zustand = ' ';
    philosophen[3].soll_gewicht = 12;
    philosophen[3].genutzt.count_2kg = 0;
    philosophen[3].genutzt.count_3kg = 0;
    philosophen[3].genutzt.count_5kg = 0;
    philosophen[3].mutex = &mutex;
    
    philosophen[4].id =  4;
    philosophen[4].befehls_status =  'n';
    philosophen[4].zustand = ' ';
    philosophen[4].soll_gewicht = 14;
    philosophen[4].genutzt.count_2kg = 0;
    philosophen[4].genutzt.count_3kg = 0;
    philosophen[4].genutzt.count_5kg = 0;
    philosophen[4].mutex = &mutex;
    
    // CV's   
    pthread_cond_init(&cond_var, NULL);    
    
    // Barriere erzeugen
    pthread_barrier_init(&barriere, NULL, NUM_THREADS);

    // Threads initialisieren
    result = 0;
    int i;
    for (i=0; i<NUM_THREADS; i++)
    {
        result = pthread_create(&philo_threads[i], NULL, philothread, &philosophen[i]);
        if (result != 0)
        {
            perror("Fehler beim erstellen des Threads");
            exit(EXIT_FAILURE);
        }
    }
    
    // Einlesen der Steuerbefehle
    while ( 1 )
    {
        // Steuerbefehle lesen
        char befehl[4];        
        fgets(befehl, 4, stdin);
        int element_1 = atoi(&befehl[0]);
        int mutex_value;
        int result;
        int exitcheck = 0;
        
        switch (befehl[0])
        {
            case 'Q':
            case 'q': // Alle Threads beenden
                      for (i=0; i<NUM_THREADS; i++)
                      {
                          steuerarray[i] = 'q';        
                      }
                      
                      for (i=0; i<NUM_THREADS; i++)
                      {   
                          steuerarray[i] = 'q';
                          sem_post(&semaphoren[ i ]);                          
                          result = pthread_join(philo_threads[i], NULL);
                          
                          if (result != 0)
                          {
                              exitcheck++;
                          }
                      }
                      
                      for (i=0; i<NUM_THREADS; i++)
                      {
                          result = sem_destroy(&semaphoren[ i ]);
                          
                          if (result != 0)
                          {
                              exitcheck++;
                          }
                      }
                      
                      result = pthread_cond_destroy(&cond_var);
                      
                      if (result != 0)
                      {
                              exitcheck++;
                      }
                      
                      result = pthread_mutex_destroy(&mutex);
                      
                      if (result != 0)
                      {
                              exitcheck++;
                      }
                      
                      exit( exitcheck );
                     
                      
                      break;
            case '0':
            case '1':
            case '2':
            case '3':
            case '4': // Funktion auf einen Thread durchführen
                      
                      if (befehl[1] == 'b')
                      {                        
                          steuerarray[ element_1 ] = 'b';
                      } 
                      else if (befehl[1] == 'u')
                      {
                          
                          sem_getvalue(&semaphoren[ element_1 ], &mutex_value);
                          
                          
                          if (mutex_value <= 0) 
                    	  {
                              result=sem_post(&semaphoren[ element_1 ]);
                              sem_getvalue(&semaphoren[ element_1 ], &mutex_value);
                          }
                      } 
                      else if (befehl[1] == 'p')
                      {
                          steuerarray[ element_1 ] = 'p';
                      }
                      break;
        }
    }
}


/*
 * Threadmethode
 * 
 */
void *philothread (void *arg)
{
    // Threaddaten
    pthread_barrier_wait(&barriere);
    struct philosoph *philosoph_data;
    philosoph_data = arg;
    
    while ( 1 )
    {
        get_weights(philosoph_data);
	    workout(philosoph_data);
	    put_weights(philosoph_data);
	    rest(philosoph_data);
    } 
}